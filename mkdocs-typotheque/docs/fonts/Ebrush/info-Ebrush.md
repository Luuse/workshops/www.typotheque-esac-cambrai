---
title: Ebrush
font_family: Ebrush
designers:
 - [Elaura Robbe, ]
styles:
 - Regular
paths:
 - fonts/Ebrush/Martine - Ebrush.otf
weight: 7
public: yes
---

