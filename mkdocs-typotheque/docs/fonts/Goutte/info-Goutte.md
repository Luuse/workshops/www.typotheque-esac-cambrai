---
title: Goutte
font_family: Goutte
designers:
 - [Emilia Bernard, ]
styles:
 - Regular
paths:
 - fonts/Goutte/Martine - Goutte.otf
weight: 7
public: yes
---
