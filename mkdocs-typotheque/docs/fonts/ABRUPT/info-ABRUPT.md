---
title: ABRUPT
font_family: ABRUPT
designers:
 - [Charlotte Delval, ]
styles:
 - Regular
paths:
 - fonts/ABRUPT/Martine - ABRUPT.otf
weight: 6
public: yes
---
