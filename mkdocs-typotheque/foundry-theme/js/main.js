var _BODY
var _BG
var _COLOR
var _ABOUT
var _TEXTS
var _SAMPLERS
var _CHARSET
var _BONBONS
var fontNames = []
var fonts
var _TITLE

var title = document.querySelector("nav h1");
var aboutButton = document.querySelector('#about_button');
var about = document.getElementById("about");
var closeAbout = about.querySelector('.close');
var closeFont = document.querySelector('.close-font');
var sizer = document.querySelector('.size');

var isInViewport = function (elem) {
	var distance = elem.getBoundingClientRect();
	return (
		distance.top >= 0 &&
		distance.left >= 0 &&
		distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		distance.right <= (window.innerWidth || document.documentElement.clientWidth)
	);
}

function changeFontSize(demos){
  var fontSizeInput = document.getElementById("fontSize");
  fontSizeInput.addEventListener("input", function(){

    var newSize = this.value;
    demos.forEach(function(demo){
      demo.style.fontSize = newSize + "px";
    });
  });
}

function loadColor() {
  var cookies = document.cookie;

  if (cookies.length > 0) {

    cookies = cookies.split(";");

    var styleElementBg = document.querySelector('head style.background');
    var styleElementColor = document.querySelector('head style.color');
    var newStyleBg = ':root{';
    var newStyleColor = ':root{';
    var bg = false;
    var color = false;
    var pickerColor = document.getElementById('textColor');
    var pickerBg = document.getElementById('backColor');
    
    cookies.forEach(function (cookie) {

      name = cookie.split("=")[0].replace(" ", "");
      value = cookie.split("=")[1].replace(" ", "");

      
      if (name == "background") {
        newStyleBg += ' --' + name + ': ' + value + '; ';
        pickerBg.setAttribute('value', value);
        bg = true
      }
      if (name == "color") { 
        newStyleColor += ' --' + name + ': ' + value + '; ';
        pickerColor.setAttribute('value', value);
        color = true
      }

    });

    newStyleBg += '}';
    newStyleColor += '}';

    if (bg == true) {
      styleElementBg.innerHTML = newStyleBg;
    }
    if (color == true) {
      styleElementColor.innerHTML = newStyleColor;
    }

  }
}

function changeColor(picker, style_type) {

  picker.addEventListener("input", function () {
    
    
    if (style_type == "background") {
    var styleElement = document.querySelector('head style.background');
    var newStyle = ':root{' + style_type + ': ' + picker.value + '; }';
    styleElement.innerHTML = newStyle;
    }

    if (style_type == "color") { 
      var styleElement = document.querySelector('head style.color');
      var newStyle = ':root{' + style_type + ': ' + picker.value + '; }';
      styleElement.innerHTML = newStyle;
    }

    document.cookie = style_type + "=" + picker.value;
  });

}

function fillSamplers(newText) {
  var divide = newText.length / _SAMPLERS.length;
  _SAMPLERS.forEach(sampler => {
    sampler.innerHTML = '';
  })
  var i = 0;
  while(newText.length) {
    var range = newText.splice(0, divide.toFixed());
    range.forEach(word => {
      if (typeof _SAMPLERS[i] !== 'undefined') {
        _SAMPLERS[i].innerHTML += word + ' ';
      }
    })
    i++;
  }
}

function candy_explosion(){
  var inactiveBonbons = document.querySelectorAll('.bonbon.inactive')
  if (inactiveBonbons.length > 0){
    let i = Math.floor(Math.random() * inactiveBonbons.length);
    let bonbon = inactiveBonbons[i]
    if(bonbon.classList.contains( 'inactive' )){
      bonbon.classList.remove('inactive')
      bonbon.classList.add('active')
    }else {
      candy_explosion()
    }
  } 
}

function candy_shop(){

	let windW = document.body.clientWidth
	let windH = document.body.offsetHeight

	_BONBONS.forEach(function(item, i){
			let bonbonLeft = Math.floor(Math.random() * (windW - 50));
			let bonbonTop = Math.floor(Math.random() * (windH - 50));
      let rotate = Math.floor(Math.random() * 360)
			item.style.left = bonbonLeft+'px';
			item.style.top = bonbonTop+'px';
			item.style.transform = 'rotate(' + rotate + 'deg)';
	})

  candy_explosion();
	setInterval(candy_explosion, 50000);

}


function showFonts(selected) {
  tags.forEach((tag) => {
    var tagValue = tag.getAttribute("data-value");
    if (tagValue == selected) {
      tag.closest(".font").classList.remove("hidden");
      tag.closest(".font").classList.add("show");
    }
  });
  fonts.forEach((font) => {
    if (!font.classList.contains("show")) {
      font.classList.remove("show");
      font.classList.add("hidden");
    }
  });
}

function generateText() {
  var allText = '';
  _TEXTS.forEach((text) => {
    var textcontent = text.innerHTML;
    allText += textcontent;
  })
  allWords = allText.split(' ');
  var newText = shuffleArray(allWords);
  return newText;
}

function shuffleArray(array) {
  let curId = array.length;
  let lastWord = '';
  if (document.body.classList.contains('mix-True')) {
    
    var articles = ['La', 'a', 'Le', 'des', 'Les', 'les', 'L\'', 'De', 'Du', 'D', 'de', 'du', 'le', 'la', 'l\'', 'une', 'un', 'à', 'se', 'ce', 'me'];
    // There remain elements to shuffle
    while (0 !== curId) {
      // Pick a remaining element
      let randId = Math.floor(Math.random() * curId);
      // Swap it with the current element.
      curId -= 1;
      let tmp = array[curId];
      if(!articles.includes(lastWord) && !articles.includes(tmp)) {
        array[curId] = array[randId];
        array[randId] = tmp;
        lastWord = tmp;
      } else {
        lastWord = tmp;
        // lastWord = lastWord
      }
    }
  }

  return array;
}

function randomTitle(){
	let titleSite = document.querySelector('#titleSite h1')
	let wordRdm = titleSite.getAttribute('data-random')
	var i = Math.floor(Math.random() * fontNames.length);
	let fontName = fontNames[i] 
	let SPAN = '<span id="martine" style="font-family: '+fontName+'">'+wordRdm+'</span>' 

	let text = titleSite.innerText.replace(wordRdm, SPAN)
	titleSite.innerHTML = text
	
}


function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}


function sortOut() {
	var fontsDict = document.getElementsByClassName('font') 
	var main = document.getElementsByClassName("main-fonts")[0]
	var divs = [];
	var l =  fontsDict.length
	const cb = document.querySelector('#sort');
	main.classList.remove('visible')

	for (var i = 0; i < l; ++i) {
		divs.push(fontsDict[i])
	}

	if (cb.classList.contains('random')) {
    cb.classList.remove('random');
    cb.innerHTML = '⤮';
		divs.sort(function(a, b) {
			return a.dataset.weight - b.dataset.weight
		});
	}else {
    cb.classList.add('random');
    cb.innerHTML = '◒';
		shuffle(divs)
	}

	main.innerHTML = ''
	divs.forEach(function(el) {
		main.innerHTML += el.outerHTML
	})

	const myTimeout = setTimeout(opacity, 200);

	function opacity() {
		main.classList.add('visible')
	}

	init()

}

function negativeFontOnHover() {
	var sampleHeaders = document.querySelectorAll('.sample-text-header');
  sampleHeaders.forEach(header => {
    header.addEventListener('mouseenter', function () {
      var parent = header.parentNode;
      parent.classList.add('hover');
      var fontColor = window.getComputedStyle(document.body).getPropertyValue("color");
      var bgColor = window.getComputedStyle(document.body).getPropertyValue("background-color");
      parent.style.color = bgColor;
      parent.style.backgroundColor = fontColor;
    })
    header.addEventListener('mouseleave', function () {
      var parent = header.parentNode;
      parent.classList.remove('hover');
      parent.style.color = '';
      parent.style.backgroundColor = '';
    })
  })
}

function focusOnFont(input, oldText) {
	var tools = document.getElementById('tester')
	tools.classList.add('hidden')
  var close = input.nextElementSibling.querySelector('.close-font');
  clickToClose(close, oldText);

  input.classList.remove('backward');
  input.classList.add('forward');
  _SAMPLERS.forEach(otherInput => {
    if (input != otherInput) {
      otherInput.classList.remove('forward');
      otherInput.classList.add('backward');
    }
  })

  var firstMod = true;
	
  input.addEventListener('paste', function (e) {
    e.preventDefault()
    var text = e.clipboardData.getData('text/plain')
    document.execCommand('insertText', false, text)
  })


  input.addEventListener("input", function(e) {
    if (firstMod == true) {
      var newText = input.innerHTML;
      var diff = findFirstDiff2(oldText, newText);
      input.innerHTML = diff;
      var range = document.createRange()
      var sel = window.getSelection()
      range.setStart(input, 1)
      range.collapse(true)
      sel.removeAllRanges()
      sel.addRange(range)
      firstMod = false;
    }
  }, false);

}

const findFirstDiff2 = function(str1, str2) {
  return str2[[...str1].findIndex(function(el, index) {
    return el !== str2[index]
  })];
}


function clickToClose(close, oldText) {
  close.addEventListener('click', (e) => {

		var tools = document.getElementById('tester')
		tools.classList.remove('hidden')
		_SAMPLERS.forEach(input => {
			if (input.classList.contains('forward')) {
				var newText = input.textContent;
				if (oldText != newText) {
					_SAMPLERS.forEach(input => {
						input.textContent = newText
					})
				}
				input.classList.remove('forward');
			}
			input.classList.remove('backward');
		})
	})
}

function lazyFont(fonts) {
  fonts.forEach(font => {
    if (isInViewport(font)) {
      var divFontFaceContent = font.querySelector('div.font-face').innerHTML;
      var styleFontFace = font.querySelector('style.font-face');
      styleFontFace.innerHTML = divFontFaceContent;
    }
  })
}

function setContentEditable(windowWidth, _SAMPLERS) {
  if (windowWidth < 600) {
    _SAMPLERS.forEach(sampler => {
      sampler.setAttribute('contenteditable', false);
    })
  } else if (windowWidth >= 600) {
    _SAMPLERS.forEach(sampler => {
      sampler.setAttribute('contenteditable', true);
    })
  }
}

function init(){

  var windowWidth = window.innerWidth;
	_SAMPLERS = document.querySelectorAll('.sample-text-edit');
  setContentEditable(windowWidth, _SAMPLERS);
  _SAMPLERS.forEach(input => {
    input.addEventListener('focus', (e) =>{
      oldText = input.textContent;
      focusOnFont(input, oldText);
    })
  })

	var inputs = document.querySelectorAll('.sample-text');
  changeFontSize(inputs);
	negativeFontOnHover()
}

document.addEventListener("DOMContentLoaded", function () {

  var windowWidth = window.innerWidth;

  _BODY = document.querySelectorAll("body");
  _ABOUT = document.querySelectorAll('#about');
  _BG = document.getElementById("backColor");
  _COLOR = document.getElementById("textColor");
	_TEXTS = document.querySelectorAll('#text-hidden li');
  _BONBONS = document.querySelectorAll('.bonbon');
	_TITLE = document.querySelectorAll('nav h1')

	sortOut('random')

	_SAMPLERS = document.querySelectorAll('.sample-text-edit');
  _CHARSET = document.querySelectorAll('.charset');

  aboutButton.addEventListener('click', function() {
    _ABOUT[0].classList.toggle('hidden');
  })

  closeAbout.addEventListener('click', function() {
    _ABOUT[0].classList.add('hidden');
  })

  var oldText

  var fonts = document.querySelectorAll(".font");

  lazyFont(fonts);

  var styles = document.querySelectorAll('.font-face')

  styles.forEach(function(item, i) {
    fontNames.push(item.getAttribute('data-font-family'))
  })

  var newText = generateText();
	fillSamplers(newText);

  changeColor(_BG, "background");
  changeColor(_COLOR, "color");
	
  loadColor();
	randomTitle()
	candy_shop()
	init()

})

window.addEventListener("resize", function () {
	_SAMPLERS = document.querySelectorAll('.sample-text-edit');

  setContentEditable(windowWidth, _SAMPLERS);
})
